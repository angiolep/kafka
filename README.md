# kafka
Docker image for GitLab Runner providing Apache Kafka 1.0.1

```bash
docker login \
  registry.gitlab.com

docker image build \
  --pull \
  --tag registry.gitlab.com/angiolep/kafka:1.0.1 \
  .

docker image push \
  registry.gitlab.com/angiolep/kafka:1.0.1
```
