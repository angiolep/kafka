FROM registry.gitlab.com/angiolep/oraclejdk:1.8.0_171
LABEL maintainer="paolo.angioletti@gmail.com"

ARG kafka_version=1.0.1
ARG kafka_package=kafka_2.11-${kafka_version}.tgz
ARG kafka_url=http://mirror.ox.ac.uk/sites/rsync.apache.org/kafka/${kafka_version}/${kafka_package}
ARG kafka_home=/opt/kafka_2.11-${kafka_version}

ENV KAFKA_VERSION=${kafka_version}
ENV KAFKA_HOME=${kafka_home}
ENV PATH=${PATH}:${KAFKA_HOME}/bin

WORKDIR ${KAFKA_HOME}

RUN set -ex && \
    apk -U upgrade && \
    apk add curl jq && \
    cd /opt && \
    printf "Installing ${kafka_package} ...\n" && \
    curl ${kafka_url} | tar -xz && \
    ln -s kafka_2.11-${kafka_version} kafka && \
    apk del curl && \
    printf "\n\n# Override settings\nzookeeper.connect=zookeeper:2181\nlisteners=PLAINTEXT://kafka:9092" >> ${kafka_home}/config/server.properties && \
    rm -Rf /tmp/* /var/cache/apk/*

# TODO Do not override configuration settings but rather provide a template file
#      whose variables will be substituted from the environment
#
#      See https://github.com/spotify/docker-kafka/blob/master/kafka/scripts/start-kafka.sh
#      See https://github.com/wurstmeister/kafka-docker/blob/master/start-kafka.sh
